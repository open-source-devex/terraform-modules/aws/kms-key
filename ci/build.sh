#!/usr/bin/env sh

terraform init -backend=false
AWS_DEFAULT_REGION=foo terraform validate .
